import { Action } from "sap/m/MessageBox"
import Table from "sap/m/Table"
import Event from "sap/ui/base/Event"
import Control from "sap/ui/core/Control"
import BaseController from "spie/hcm/jupiter/base/controller/Controller"
import Fragment from "sap/ui/core/Fragment"
/**
 * @namespace spie.hcm.jupiter.main.controller
 */
class Master extends BaseController {
    _oTable: Table
    _oViewModel: null

    public onInit() {
        BaseController.prototype.onInit.apply(this, arguments)
        this.getView().setModel(this._oViewModel, "masterView")
        this._initElements()
        this._loadData()

        Fragment.load({
            id: this._oView.getId(),
            name: "spie.hcm.jupiter.fragments.Example",
            controller: this
        }).then((oFragment) => {
            this._oView.addDependent(oFragment)
            this._oView.byId("fragmentBox").addItem(oFragment)
        })
    }

    private _initElements() {
        this._oTable = this.getElement("idTable")
    }

    private async _loadData() {
        try {
            await this.getODataModel().metadataLoaded()
        } catch (err) {
        } finally {
        }
    }

    protected async _onShowDetails(oEvent: Event) {
        this._oTable.setBusy(true) //for the sake of that example
        this._oTable.setBusy(false)
    }

    protected _createNewCustomer() {}
}
