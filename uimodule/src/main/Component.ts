import BaseComponent from "spie/hcm/jupiter/base/Component"
import Device from "sap/ui/Device"
import JSONModel from "sap/ui/model/json/JSONModel"

import ResourceBundle from "sap/base/i18n/ResourceBundle"
import ResourceModel from "sap/ui/model/resource/ResourceModel"
import ODataModel from "sap/ui/model/odata/v2/ODataModel"

/**
 * @namespace spie.hcm.jupiter.main
 */
export default class Component extends BaseComponent {
    public static metadata = {
        manifest: "json"
    }

    init() {
        super.init()

        this._initCustomData()
        this._initComponentStyles()
    }

    protected _loadCss(id: any, url: any) {
        var cssId = id // you could encode the css path itself to generate id..
        if (!document.getElementById(cssId)) {
            var head = document.getElementsByTagName("head")[0]
            var link = document.createElement("link")
            link.id = cssId
            link.rel = "stylesheet"
            link.type = "text/css"
            link.href = url
            link.media = "all"
            head.appendChild(link)
        }
    }

    protected _initComponentStyles() {
        throw new Error("Method not implemented.")
    }

    protected _initCustomData() {
        throw new Error("Method not implemented.")
    }
}
