import Device from "sap/ui/Device"
import JSONModel from "sap/ui/model/json/JSONModel"

export default class ComponentModel {
    createEssModel() {
        const data = {
            bool: true,
            text: "test",
            color: "Highlight"
        }
        var oModel = new JSONModel(data)
        oModel.setDefaultBindingMode("OneWay")
        return oModel
    }
}
