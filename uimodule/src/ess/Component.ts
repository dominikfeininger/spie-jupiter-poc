import MainComponent from "spie/hcm/jupiter/main/Component"
import Device from "sap/ui/Device"
import JSONModel from "sap/ui/model/json/JSONModel"
import ResourceBundle from "sap/base/i18n/ResourceBundle"
import ResourceModel from "sap/ui/model/resource/ResourceModel"
import ODataModel from "sap/ui/model/odata/v2/ODataModel"
import ComponentModel from "./model/ComponentModel"

/**
 * @namespace spie.hcm.jupiter.ess
 */
export default class Component extends MainComponent {
    protected _initCustomData() {
        this.setModel(new ComponentModel().createEssModel(), "componentModel")
    }

    protected _initComponentStyles() {
        this._loadCss("compAppLib", jQuery.sap.getResourcePath("spie/hcm/jupiter/ess") + "/styles/styles.css")
    }
}
