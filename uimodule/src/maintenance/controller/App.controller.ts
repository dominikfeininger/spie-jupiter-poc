import Event from "sap/ui/base/Event"
import View from "sap/ui/core/mvc/View"
import ODataModel from "sap/ui/model/odata/v2/ODataModel"
import BaseController from "spie/hcm/jupiter/base/controller/Controller"

/**
 * @namespace spie.hcm.jupiter.maintenence.controller
 */
class App extends BaseController {
    _oModel: ODataModel
    _prop: String = "null"

    onInit() {
        this._prop = "test super"
        this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass())
        this._initViewModel()
        // test
        this._initBusy()
    }

    private _initViewModel() {}

    private async _initBusy() {
        await this.getOwnerComponent().getODataModel().metadataLoaded()
    }
}
