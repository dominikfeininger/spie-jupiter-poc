import BaseComponent from "spie/hcm/jupiter/base/Component"
import Device from "sap/ui/Device"
import JSONModel from "sap/ui/model/json/JSONModel"

import ResourceBundle from "sap/base/i18n/ResourceBundle"
import ResourceModel from "sap/ui/model/resource/ResourceModel"
import ODataModel from "sap/ui/model/odata/v2/ODataModel"

/**
 * @namespace spie.hcm.jupiter.maintenence
 */
export default class Component extends BaseComponent {
    public static metadata = {
        manifest: "json"
    }

    init() {
        super.init()

        this.setModel(new JSONModel(Device), "device")
        this.getRouter().initialize()
    }
}
