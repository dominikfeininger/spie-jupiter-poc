import JSONModel from "sap/ui/model/json/JSONModel"

export default class ComponentModel {
    createMaintenanceModel() {
        const data = {
            bool: true,
            text: "test",
            color: "Highlight"
        }
        var oModel = new JSONModel(data)
        oModel.setDefaultBindingMode("OneWay")
        return oModel
    }
}
