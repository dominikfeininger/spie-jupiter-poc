import UIComponent from "sap/ui/core/UIComponent"
import Device from "sap/ui/Device"
import JSONModel from "sap/ui/model/json/JSONModel"

import ResourceBundle from "sap/base/i18n/ResourceBundle"
import ResourceModel from "sap/ui/model/resource/ResourceModel"
import ODataModel from "sap/ui/model/odata/v2/ODataModel"

/**
 * @namespace spie.hcm.jupiter.base
 */
export default class BaseComponent extends UIComponent {
    init() {
        super.init()

        this.setModel(new JSONModel(Device), "device")
        this.getRouter().initialize()
    }

    exit() {}

    getResourceBundle(): ResourceBundle {
        const resourceModel = <ResourceModel>this.getModel("i18n")
        return resourceModel.getResourceBundle() as ResourceBundle
    }

    getContentDensityClass() {
        return "sapUiSizeCompact"
    }

    getAppViewModel(): JSONModel {
        return this.getModel("appView") as JSONModel
    }

    getODataModel(): ODataModel {
        return this.getModel() as ODataModel
    }

    destroy(bSuppressInvalidate?: boolean) {
        super.destroy.apply(this, [bSuppressInvalidate])
    }
}
