/*!
 * ${copyright}
 */
// Provides control spie.hcm.controls.Example.
sap.ui.define(["./library", "sap/ui/core/Control", "./ExampleRenderer"], function (library, Control, ExampleRenderer) {
  "use strict"; // refer to library types

  var ExampleColor = library.ExampleColor;
  /**
   * Constructor for a new <code>spie.hcm.controls.Example</code> control.
   *
   * @param {string} [sId] id for the new control, generated automatically if no id is given
   * @param {object} [mSettings] initial settings for the new control
   *
   * @class
   * Some class description goes here.
   * @extends sap.ui.core.Control
   *
   * @author dominik.feininger
   * @version ${version}
   *
   * @constructor
   * @public
   * @alias spie.hcm.controls.Example
   */

  var Example = Control.extend("spie.hcm.controls.Example",
  /** @lends spie.hcm.controls.Example.prototype */
  {
    metadata: {
      library: "spie.hcm.controls",
      properties: {
        /**
         * The text to display.
         */
        text: {
          type: "string",
          group: "Data",
          defaultValue: null
        },

        /**
         * The color to use (default to "Default" color).
         */
        color: {
          type: "spie.hcm.controls.ExampleColor",
          group: "Appearance",
          defaultValue: ExampleColor.Default
        }
      },
      events: {
        /**
         * Event is fired when the user clicks the control.
         */
        press: {}
      }
    },
    renderer: ExampleRenderer,
    onclick: function () {
      this.firePress();
    }
  });
  return Example;
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9zcGllLmhjbS5jb250cm9scy9zcmMvc3BpZS9oY20vY29udHJvbHMvRXhhbXBsZS5qcyJdLCJuYW1lcyI6WyJzYXAiLCJ1aSIsImRlZmluZSIsImxpYnJhcnkiLCJDb250cm9sIiwiRXhhbXBsZVJlbmRlcmVyIiwiRXhhbXBsZUNvbG9yIiwiRXhhbXBsZSIsImV4dGVuZCIsIm1ldGFkYXRhIiwicHJvcGVydGllcyIsInRleHQiLCJ0eXBlIiwiZ3JvdXAiLCJkZWZhdWx0VmFsdWUiLCJjb2xvciIsIkRlZmF1bHQiLCJldmVudHMiLCJwcmVzcyIsInJlbmRlcmVyIiwib25jbGljayIsImZpcmVQcmVzcyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQUEsR0FBRyxDQUFDQyxFQUFKLENBQU9DLE1BQVAsQ0FBYyxDQUNiLFdBRGEsRUFFYixxQkFGYSxFQUdiLG1CQUhhLENBQWQsRUFJRyxVQUFVQyxPQUFWLEVBQW1CQyxPQUFuQixFQUE0QkMsZUFBNUIsRUFBNkM7QUFDL0MsZUFEK0MsQ0FHL0M7O0FBQ0EsTUFBSUMsWUFBWSxHQUFHSCxPQUFPLENBQUNHLFlBQTNCO0FBRUE7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJQyxPQUFPLEdBQUdILE9BQU8sQ0FBQ0ksTUFBUixDQUFlLDJCQUFmO0FBQTRDO0FBQWtEO0FBQzNHQyxJQUFBQSxRQUFRLEVBQUU7QUFDVE4sTUFBQUEsT0FBTyxFQUFFLG1CQURBO0FBRVRPLE1BQUFBLFVBQVUsRUFBRTtBQUNYO0FBQ0o7QUFDQTtBQUNJQyxRQUFBQSxJQUFJLEVBQUU7QUFDTEMsVUFBQUEsSUFBSSxFQUFFLFFBREQ7QUFFTEMsVUFBQUEsS0FBSyxFQUFFLE1BRkY7QUFHTEMsVUFBQUEsWUFBWSxFQUFFO0FBSFQsU0FKSzs7QUFTWDtBQUNKO0FBQ0E7QUFDSUMsUUFBQUEsS0FBSyxFQUFFO0FBQ05ILFVBQUFBLElBQUksRUFBRSxnQ0FEQTtBQUVOQyxVQUFBQSxLQUFLLEVBQUUsWUFGRDtBQUdOQyxVQUFBQSxZQUFZLEVBQUVSLFlBQVksQ0FBQ1U7QUFIckI7QUFaSSxPQUZIO0FBb0JUQyxNQUFBQSxNQUFNLEVBQUU7QUFDUDtBQUNKO0FBQ0E7QUFDSUMsUUFBQUEsS0FBSyxFQUFFO0FBSkE7QUFwQkMsS0FEaUc7QUE0QjNHQyxJQUFBQSxRQUFRLEVBQUVkLGVBNUJpRztBQTZCekdlLElBQUFBLE9BQU8sRUFBRSxZQUFXO0FBQ2xCLFdBQUtDLFNBQUw7QUFDRDtBQS9Cd0csR0FBOUYsQ0FBZDtBQWlDQSxTQUFPZCxPQUFQO0FBRUEsQ0E5REQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICR7Y29weXJpZ2h0fVxuICovXG5cbi8vIFByb3ZpZGVzIGNvbnRyb2wgc3BpZS5oY20uY29udHJvbHMuRXhhbXBsZS5cbnNhcC51aS5kZWZpbmUoW1xuXHRcIi4vbGlicmFyeVwiLCBcblx0XCJzYXAvdWkvY29yZS9Db250cm9sXCIsIFxuXHRcIi4vRXhhbXBsZVJlbmRlcmVyXCJcbl0sIGZ1bmN0aW9uIChsaWJyYXJ5LCBDb250cm9sLCBFeGFtcGxlUmVuZGVyZXIpIHtcblx0XCJ1c2Ugc3RyaWN0XCI7XG5cblx0Ly8gcmVmZXIgdG8gbGlicmFyeSB0eXBlc1xuXHR2YXIgRXhhbXBsZUNvbG9yID0gbGlicmFyeS5FeGFtcGxlQ29sb3I7XG5cblx0LyoqXG5cdCAqIENvbnN0cnVjdG9yIGZvciBhIG5ldyA8Y29kZT5zcGllLmhjbS5jb250cm9scy5FeGFtcGxlPC9jb2RlPiBjb250cm9sLlxuXHQgKlxuXHQgKiBAcGFyYW0ge3N0cmluZ30gW3NJZF0gaWQgZm9yIHRoZSBuZXcgY29udHJvbCwgZ2VuZXJhdGVkIGF1dG9tYXRpY2FsbHkgaWYgbm8gaWQgaXMgZ2l2ZW5cblx0ICogQHBhcmFtIHtvYmplY3R9IFttU2V0dGluZ3NdIGluaXRpYWwgc2V0dGluZ3MgZm9yIHRoZSBuZXcgY29udHJvbFxuXHQgKlxuXHQgKiBAY2xhc3Ncblx0ICogU29tZSBjbGFzcyBkZXNjcmlwdGlvbiBnb2VzIGhlcmUuXG5cdCAqIEBleHRlbmRzIHNhcC51aS5jb3JlLkNvbnRyb2xcblx0ICpcblx0ICogQGF1dGhvciBkb21pbmlrLmZlaW5pbmdlclxuXHQgKiBAdmVyc2lvbiAke3ZlcnNpb259XG5cdCAqXG5cdCAqIEBjb25zdHJ1Y3RvclxuXHQgKiBAcHVibGljXG5cdCAqIEBhbGlhcyBzcGllLmhjbS5jb250cm9scy5FeGFtcGxlXG5cdCAqL1xuXHR2YXIgRXhhbXBsZSA9IENvbnRyb2wuZXh0ZW5kKFwic3BpZS5oY20uY29udHJvbHMuRXhhbXBsZVwiLCAvKiogQGxlbmRzIHNwaWUuaGNtLmNvbnRyb2xzLkV4YW1wbGUucHJvdG90eXBlICovIHtcblx0XHRtZXRhZGF0YToge1xuXHRcdFx0bGlicmFyeTogXCJzcGllLmhjbS5jb250cm9sc1wiLFxuXHRcdFx0cHJvcGVydGllczoge1xuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogVGhlIHRleHQgdG8gZGlzcGxheS5cblx0XHRcdFx0ICovXG5cdFx0XHRcdHRleHQ6IHtcblx0XHRcdFx0XHR0eXBlOiBcInN0cmluZ1wiLFxuXHRcdFx0XHRcdGdyb3VwOiBcIkRhdGFcIixcblx0XHRcdFx0XHRkZWZhdWx0VmFsdWU6IG51bGxcblx0XHRcdFx0fSxcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIFRoZSBjb2xvciB0byB1c2UgKGRlZmF1bHQgdG8gXCJEZWZhdWx0XCIgY29sb3IpLlxuXHRcdFx0XHQgKi9cblx0XHRcdFx0Y29sb3I6IHtcblx0XHRcdFx0XHR0eXBlOiBcInNwaWUuaGNtLmNvbnRyb2xzLkV4YW1wbGVDb2xvclwiLFxuXHRcdFx0XHRcdGdyb3VwOiBcIkFwcGVhcmFuY2VcIixcblx0XHRcdFx0XHRkZWZhdWx0VmFsdWU6IEV4YW1wbGVDb2xvci5EZWZhdWx0XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRldmVudHM6IHtcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIEV2ZW50IGlzIGZpcmVkIHdoZW4gdGhlIHVzZXIgY2xpY2tzIHRoZSBjb250cm9sLlxuXHRcdFx0XHQgKi9cblx0XHRcdFx0cHJlc3M6IHt9XG5cdFx0XHR9XG5cdFx0fSxcblx0XHRyZW5kZXJlcjogRXhhbXBsZVJlbmRlcmVyLFxuICAgIG9uY2xpY2s6IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5maXJlUHJlc3MoKTtcbiAgICB9XG5cdH0pO1xuXHRyZXR1cm4gRXhhbXBsZTtcblxufSk7XG4iXX0=