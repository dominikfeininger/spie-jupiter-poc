/*!
 * ${copyright}
 */

/**
 * Initialization Code and shared classes of library spie.hcm.controls.
 */
sap.ui.define(["sap/ui/core/library"], function () {
  "use strict"; // delegate further initialization of this library to the Core
  // Hint: sap.ui.getCore() must still be used to support preload with sync bootstrap!

  sap.ui.getCore().initLibrary({
    name: "spie.hcm.controls",
    version: "${version}",
    dependencies: [// keep in sync with the ui5.yaml and .library files
    "sap.ui.core"],
    types: ["spie.hcm.controls.ExampleColor"],
    interfaces: [],
    controls: ["spie.hcm.controls.Example"],
    elements: [],
    noLibraryCSS: false // if no CSS is provided, you can disable the library.css load here

  });
  /**
   * Some description about <code>controls</code>
   *
   * @namespace
   * @name spie.hcm.controls
   * @author dominik.feininger
   * @version ${version}
   * @public
   */

  var thisLib = spie.hcm.controls;
  /**
   * Semantic Colors of the <code>spie.hcm.controls.Example</code>.
   *
   * @enum {string}
   * @public
   */

  thisLib.ExampleColor = {
    /**
     * Default color (brand color)
     * @public
     */
    Default: "Default",

    /**
     * Highlight color
     * @public
     */
    Highlight: "Highlight"
  };
  return thisLib;
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9zcGllLmhjbS5jb250cm9scy9zcmMvc3BpZS9oY20vY29udHJvbHMvbGlicmFyeS5qcyJdLCJuYW1lcyI6WyJzYXAiLCJ1aSIsImRlZmluZSIsImdldENvcmUiLCJpbml0TGlicmFyeSIsIm5hbWUiLCJ2ZXJzaW9uIiwiZGVwZW5kZW5jaWVzIiwidHlwZXMiLCJpbnRlcmZhY2VzIiwiY29udHJvbHMiLCJlbGVtZW50cyIsIm5vTGlicmFyeUNTUyIsInRoaXNMaWIiLCJzcGllIiwiaGNtIiwiRXhhbXBsZUNvbG9yIiwiRGVmYXVsdCIsIkhpZ2hsaWdodCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBQSxHQUFHLENBQUNDLEVBQUosQ0FBT0MsTUFBUCxDQUFjLENBQ2IscUJBRGEsQ0FBZCxFQUVHLFlBQVk7QUFDZCxlQURjLENBR2Q7QUFDQTs7QUFDQUYsRUFBQUEsR0FBRyxDQUFDQyxFQUFKLENBQU9FLE9BQVAsR0FBaUJDLFdBQWpCLENBQTZCO0FBQzVCQyxJQUFBQSxJQUFJLEVBQUUsbUJBRHNCO0FBRTVCQyxJQUFBQSxPQUFPLEVBQUUsWUFGbUI7QUFHNUJDLElBQUFBLFlBQVksRUFBRSxDQUFFO0FBQ2YsaUJBRGEsQ0FIYztBQU01QkMsSUFBQUEsS0FBSyxFQUFFLENBQ04sZ0NBRE0sQ0FOcUI7QUFTNUJDLElBQUFBLFVBQVUsRUFBRSxFQVRnQjtBQVU1QkMsSUFBQUEsUUFBUSxFQUFFLENBQ1QsMkJBRFMsQ0FWa0I7QUFhNUJDLElBQUFBLFFBQVEsRUFBRSxFQWJrQjtBQWM1QkMsSUFBQUEsWUFBWSxFQUFFLEtBZGMsQ0FjUjs7QUFkUSxHQUE3QjtBQWlCQTtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSUMsT0FBTyxHQUFHQyxJQUFJLENBQUNDLEdBQUwsQ0FBU0wsUUFBdkI7QUFFQTtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0NHLEVBQUFBLE9BQU8sQ0FBQ0csWUFBUixHQUF1QjtBQUV0QjtBQUNGO0FBQ0E7QUFDQTtBQUNFQyxJQUFBQSxPQUFPLEVBQUcsU0FOWTs7QUFRdEI7QUFDRjtBQUNBO0FBQ0E7QUFDRUMsSUFBQUEsU0FBUyxFQUFHO0FBWlUsR0FBdkI7QUFnQkEsU0FBT0wsT0FBUDtBQUVBLENBM0REIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiAke2NvcHlyaWdodH1cbiAqL1xuXG4vKipcbiAqIEluaXRpYWxpemF0aW9uIENvZGUgYW5kIHNoYXJlZCBjbGFzc2VzIG9mIGxpYnJhcnkgc3BpZS5oY20uY29udHJvbHMuXG4gKi9cbnNhcC51aS5kZWZpbmUoW1xuXHRcInNhcC91aS9jb3JlL2xpYnJhcnlcIlxuXSwgZnVuY3Rpb24gKCkge1xuXHRcInVzZSBzdHJpY3RcIjtcblxuXHQvLyBkZWxlZ2F0ZSBmdXJ0aGVyIGluaXRpYWxpemF0aW9uIG9mIHRoaXMgbGlicmFyeSB0byB0aGUgQ29yZVxuXHQvLyBIaW50OiBzYXAudWkuZ2V0Q29yZSgpIG11c3Qgc3RpbGwgYmUgdXNlZCB0byBzdXBwb3J0IHByZWxvYWQgd2l0aCBzeW5jIGJvb3RzdHJhcCFcblx0c2FwLnVpLmdldENvcmUoKS5pbml0TGlicmFyeSh7XG5cdFx0bmFtZTogXCJzcGllLmhjbS5jb250cm9sc1wiLFxuXHRcdHZlcnNpb246IFwiJHt2ZXJzaW9ufVwiLFxuXHRcdGRlcGVuZGVuY2llczogWyAvLyBrZWVwIGluIHN5bmMgd2l0aCB0aGUgdWk1LnlhbWwgYW5kIC5saWJyYXJ5IGZpbGVzXG5cdFx0XHRcInNhcC51aS5jb3JlXCJcblx0XHRdLFxuXHRcdHR5cGVzOiBbXG5cdFx0XHRcInNwaWUuaGNtLmNvbnRyb2xzLkV4YW1wbGVDb2xvclwiXG5cdFx0XSxcblx0XHRpbnRlcmZhY2VzOiBbXSxcblx0XHRjb250cm9sczogW1xuXHRcdFx0XCJzcGllLmhjbS5jb250cm9scy5FeGFtcGxlXCJcblx0XHRdLFxuXHRcdGVsZW1lbnRzOiBbXSxcblx0XHRub0xpYnJhcnlDU1M6IGZhbHNlIC8vIGlmIG5vIENTUyBpcyBwcm92aWRlZCwgeW91IGNhbiBkaXNhYmxlIHRoZSBsaWJyYXJ5LmNzcyBsb2FkIGhlcmVcblx0fSk7XG5cblx0LyoqXG5cdCAqIFNvbWUgZGVzY3JpcHRpb24gYWJvdXQgPGNvZGU+Y29udHJvbHM8L2NvZGU+XG5cdCAqXG5cdCAqIEBuYW1lc3BhY2Vcblx0ICogQG5hbWUgc3BpZS5oY20uY29udHJvbHNcblx0ICogQGF1dGhvciBkb21pbmlrLmZlaW5pbmdlclxuXHQgKiBAdmVyc2lvbiAke3ZlcnNpb259XG5cdCAqIEBwdWJsaWNcblx0ICovXG5cdHZhciB0aGlzTGliID0gc3BpZS5oY20uY29udHJvbHM7XG5cblx0LyoqXG5cdCAqIFNlbWFudGljIENvbG9ycyBvZiB0aGUgPGNvZGU+c3BpZS5oY20uY29udHJvbHMuRXhhbXBsZTwvY29kZT4uXG5cdCAqXG5cdCAqIEBlbnVtIHtzdHJpbmd9XG5cdCAqIEBwdWJsaWNcblx0ICovXG5cdHRoaXNMaWIuRXhhbXBsZUNvbG9yID0ge1xuXG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBjb2xvciAoYnJhbmQgY29sb3IpXG5cdFx0ICogQHB1YmxpY1xuXHRcdCAqL1xuXHRcdERlZmF1bHQgOiBcIkRlZmF1bHRcIixcblxuXHRcdC8qKlxuXHRcdCAqIEhpZ2hsaWdodCBjb2xvclxuXHRcdCAqIEBwdWJsaWNcblx0XHQgKi9cblx0XHRIaWdobGlnaHQgOiBcIkhpZ2hsaWdodFwiXG5cblx0fTtcblxuXHRyZXR1cm4gdGhpc0xpYjtcblxufSk7XG4iXX0=