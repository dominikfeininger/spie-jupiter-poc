# jupiter

Insert the purpose of this project and some interesting infos here

## Typescript

Typescript Demo with UI5. This demo should give you a minimal idea, of what is possible using UI5 and typescript.

To run execute `npm run start`.

[Typescript](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html) Documentation

## Credits

This project has been generated with 💙 and [easy-ui5](https://github.com/SAP)
